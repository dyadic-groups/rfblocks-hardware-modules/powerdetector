# An LTC5582 Based Power Detector

## Features

- Broadband performance: $\pm 1.0$ dB over the range 0.1 to 4 GHz.
- Linear dynamic range of 50 dB over the range 0.1 to 4 GHz.
- Usable to over 6 GHz with reduced dynamic range.
- On board EEPROM for calibration parameters.
- Optional access to detector output voltage.

## Documentation

Full documentation for the detector is available at
[RF Blocks](https://rfblocks.org/boards/LTC5582-Power-Detector.html)

## License

[CERN-OHL-W v2.](https://ohwr.org/project/cernohl/wikis/Documents/CERN-OHL-version-2)
